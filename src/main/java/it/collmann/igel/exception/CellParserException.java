package it.collmann.igel.exception;

@SuppressWarnings("serial")
public class CellParserException extends RuntimeException {

	public CellParserException(String errorMessage) {
		super(errorMessage);
	}
}
