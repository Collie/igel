package it.collmann.igel.parser;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.collmann.igel.model.CellGroup;

public class CellMatchingParser {
	public static void parseMatchingToFolder(Map<Integer, List<Integer>> cellMatching, CellGroup group1,
			CellGroup group2, Path path) {

		List<String> mappings = new ArrayList<>();
		for (Integer cellNum : cellMatching.keySet()) {
			for (Integer value : cellMatching.get(cellNum)) {
				if (value != null) {
					mappings.add(String.valueOf(cellNum) + "," + String.valueOf(value) + "\n");
				}
			}
		}

		Path directoryForMatching = Paths.get(path.toString(), group1.getName(),
				"Solution_" + group1.getName() + ".csv");

		try {
			Files.createDirectories(directoryForMatching);
			Files.deleteIfExists(directoryForMatching);

			FileOutputStream outputStreamAllPoints = new FileOutputStream(directoryForMatching.toString());
			for (String s : mappings) {
				byte bytes[] = s.getBytes();
				outputStreamAllPoints.write(bytes);
			}
			outputStreamAllPoints.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
