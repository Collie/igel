package it.collmann.igel.parser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import it.collmann.igel.exception.CellParserException;
import it.collmann.igel.model.Cell;
import it.collmann.igel.model.CellConnection;
import it.collmann.igel.model.CellGroup;
import it.collmann.igel.model.Point;
import it.collmann.igel.model.Triangle;

/**
 * Provides functionality to parse .xlxs files into CellGroups and create .xlxs
 * files from CellGroups.
 * 
 * @author collie
 *
 */
public final class CellGroupParser {
	/**
	 * 
	 * @param folderPath A path to a directory that contains each one file of
	 *                   *name*AllPoints.xlxs and *name*FinalFaces.xlsx
	 * @return A cell group containing all cells defined in the excel files
	 *         contained in the given directory.
	 */
	public static Optional<CellGroup> parseCellGroupFromFolder(String directoryPath) {
		Optional<Sheet> finalFaces = getFirstSheetFromWorkbookByName(directoryPath, "FinalFaces");
		Optional<Sheet> allPoints = getFirstSheetFromWorkbookByName(directoryPath, "AllPoints");

		Set<Point> pointsWithCellIds = new HashSet<>();
		CellGroup cellGroup = null;

		if (finalFaces.isPresent() && allPoints.isPresent()) {
			List<Point> points = new ArrayList<>();
			for (Row row : allPoints.get()) {
				int id = row.getRowNum() + 1;
				int x = (int) row.getCell(0).getNumericCellValue();
				int y = (int) row.getCell(1).getNumericCellValue();
				int z = (int) row.getCell(2).getNumericCellValue();
				points.add(new Point(id, x, y, z));
			}

			List<Cell> cells = new ArrayList<>();

			Cell currentCell = new Cell((int) finalFaces.get().getRow(0).getCell(3).getNumericCellValue(),
					new ArrayList<>(), null);

			for (Row row : finalFaces.get()) {
				if (currentCell.getId() != (int) row.getCell(3).getNumericCellValue()) {

					// only add cell to cellGroup if it has more than two triangles
					if (currentCell.getTriangles().size() >= 3) {
						currentCell
								.setCenter(findCenterPointOfTriangles(currentCell.getTriangles(), currentCell.getId()));
						cells.add(currentCell);
					}
					currentCell = new Cell((int) row.getCell(3).getNumericCellValue(), new ArrayList<>(), null);
				}

				Point p1 = findPointById(points, (int) row.getCell(0).getNumericCellValue());
				p1.addCellReference(currentCell.getId());
				Point p2 = findPointById(points, (int) row.getCell(1).getNumericCellValue());
				p2.addCellReference(currentCell.getId());
				Point p3 = findPointById(points, (int) row.getCell(2).getNumericCellValue());
				p3.addCellReference(currentCell.getId());

				pointsWithCellIds.add(p1);
				pointsWithCellIds.add(p2);
				pointsWithCellIds.add(p3);

				currentCell.addTriangle(new Triangle(p1, p2, p3));
			}
			// save last cell
			currentCell.setCenter(findCenterPointOfTriangles(currentCell.getTriangles(), currentCell.getId()));
			cells.add(currentCell);

			File directory = new File(directoryPath);
			String cellGroupName = directory.getName();

			cellGroup = new CellGroup(cells, cellGroupName, findAllCellConnections(pointsWithCellIds, cells),
					new HashSet<Point>(points), 0);
		}

		return Optional.ofNullable(cellGroup);
	}

	/**
	 * Creates a List of all connections to this cell by finding the connected
	 * points of cells.
	 * 
	 * @param cells
	 * @return
	 */
	public static List<CellConnection> findAllCellConnections(Set<Point> points, List<Cell> cells) {
		List<CellConnection> connections = new ArrayList<>();

		for (Point point : points) {
			if (point.getReferencedCellIds().size() >= 2) {
				int firstCellId = point.getReferencedCellIds().stream().findFirst().get();
				point.getReferencedCellIds().remove(firstCellId);
				for (Integer i : point.getReferencedCellIds()) {
					Cell cell1 = cells.stream().filter(cell -> firstCellId == cell.getId()).findFirst().orElse(null);
					Cell cell2 = cells.stream().filter(cell -> i == cell.getId()).findFirst().orElse(null);
					if (!connectionExists(cell1, cell2, connections) && !cell1.equals(cell2)) {
						if (cell2 == null) {
							continue;
						}
						connections.add(new CellConnection(cell1, cell2));
					}
				}

			}
		}

		return connections;
	}

	private static boolean connectionExists(Cell cell1, Cell cell2, List<CellConnection> connections) {
		boolean connectionExists = false;
		for (CellConnection connection : connections) {
			boolean cell1EqualsOneFromConnection = connection.getCell1().equals(cell1)
					|| connection.getCell1().equals(cell2);
			boolean cell2EqualsOneFromConnection = connection.getCell2().equals(cell1)
					|| connection.getCell2().equals(cell2);

			if (cell1EqualsOneFromConnection && cell2EqualsOneFromConnection) {
				connectionExists = true;
			}
		}
		return connectionExists;
	}

	/**
	 * Parses a CellGroup to target/generated/[Name of CellGroup]/ with files
	 * [NameOfCellGroup]_AllPoints.xlsx and [Name of CellGroup]_FinalFaces.xlsx.
	 * 
	 * @param cellGroup A cellGroup to be parsed into AllPoints.xlsx and
	 *                  FinalFaces.xlsx files
	 */
	public static void parseCellGroupToFolder(CellGroup cellGroup, Path path) {
		Workbook allPointsWorkbook = new XSSFWorkbook();
		Workbook finalFacesWorkbook = new XSSFWorkbook();

		Sheet allPointsSheet = allPointsWorkbook.createSheet();
		Sheet finalFacesSheet = finalFacesWorkbook.createSheet();

		int rowCounterForFinalFaces = 0;
		for (Cell cell : cellGroup.getCells()) {
			for (Triangle triangle : cell.getTriangles()) {
				// PARSE FINALFACES
				Row finalFacesCurrentRow = finalFacesSheet.createRow(rowCounterForFinalFaces);
				org.apache.poi.ss.usermodel.Cell finalFacesRowCell1 = finalFacesCurrentRow.createCell(0);
				org.apache.poi.ss.usermodel.Cell finalFacesRowCell2 = finalFacesCurrentRow.createCell(1);
				org.apache.poi.ss.usermodel.Cell finalFacesRowCell3 = finalFacesCurrentRow.createCell(2);
				org.apache.poi.ss.usermodel.Cell finalFacesRowCell4 = finalFacesCurrentRow.createCell(3);

				finalFacesRowCell1.setCellValue(triangle.getPoints().get(0).getId());
				finalFacesRowCell2.setCellValue(triangle.getPoints().get(1).getId());
				finalFacesRowCell3.setCellValue(triangle.getPoints().get(2).getId());
				finalFacesRowCell4.setCellValue(cell.getId());

				rowCounterForFinalFaces++;

				// PARSE ALLPOINTS
				Row allPointsRow1 = allPointsSheet.createRow(triangle.getPoints().get(0).getId() - 1);
				allPointsRow1.createCell(0).setCellValue(triangle.getPoints().get(0).getX());
				allPointsRow1.createCell(1).setCellValue(triangle.getPoints().get(0).getY());
				allPointsRow1.createCell(2).setCellValue(triangle.getPoints().get(0).getZ());

				Row allPointsRow2 = allPointsSheet.createRow(triangle.getPoints().get(1).getId() - 1);
				allPointsRow2.createCell(0).setCellValue(triangle.getPoints().get(1).getX());
				allPointsRow2.createCell(1).setCellValue(triangle.getPoints().get(1).getY());
				allPointsRow2.createCell(2).setCellValue(triangle.getPoints().get(1).getZ());

				Row allPointsRow3 = allPointsSheet.createRow(triangle.getPoints().get(2).getId() - 1);
				allPointsRow3.createCell(0).setCellValue(triangle.getPoints().get(2).getX());
				allPointsRow3.createCell(1).setCellValue(triangle.getPoints().get(2).getY());
				allPointsRow3.createCell(2).setCellValue(triangle.getPoints().get(2).getZ());

			}
		}
		Path directoryForFinalFaces = Paths.get(path.toString(), cellGroup.getName(),
				cellGroup.getName() + "_" + cellGroup.getHours().toString(),
				cellGroup.getName() + "_" + cellGroup.getHours().toString() + "_FinalFaces.xlsx");

		Path directoryForAllPoints = Paths.get(path.toString(), cellGroup.getName(),
				cellGroup.getName() + "_" + cellGroup.getHours().toString(),
				cellGroup.getName() + "_" + cellGroup.getHours().toString() + "_AllPoints.xlsx");

		String directoryForFinalFacesString = directoryForFinalFaces.toFile().getAbsolutePath();
		String directoryForAllPointsString = directoryForAllPoints.toFile().getAbsolutePath();

		try {

			Files.createDirectories(directoryForFinalFaces);
			Files.createDirectories(directoryForAllPoints);
			Files.deleteIfExists(directoryForAllPoints);
			Files.deleteIfExists(directoryForFinalFaces);

			FileOutputStream outputStreamAllPoints = new FileOutputStream(directoryForAllPointsString);
			FileOutputStream outputStreamFinalFaces = new FileOutputStream(directoryForFinalFacesString);

			allPointsWorkbook.write(outputStreamAllPoints);
			finalFacesWorkbook.write(outputStreamFinalFaces);

			allPointsWorkbook.close();
			finalFacesWorkbook.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Parses all CellGroups from the main resource folder for later usage.
	 * 
	 * @return A List of all CellGroups that could be parsed from the given folder
	 *         where names contain "_0h" oder "_Before"
	 */
	public static List<CellGroup> parseAllInitialCellGroupsFromParentFolder() {
		File allResources = Paths.get("src", "resources", "TestInstances").toFile();
		String[] subdirectories = allResources.list();

		List<String> dataDirectories = new ArrayList<>();
		for (String subdir : subdirectories) {
			File subDirectories = Paths.get("src", "resources", "TestInstances", subdir).toFile();
			String smallestDirectory = subDirectories.list(new FilenameFilter() {
				@Override
				public boolean accept(File current, String name) {
					return (name.endsWith("_0h") || name.endsWith("_Before"));
				}
			})[0];
			dataDirectories.add(Paths.get("src", "resources", "TestInstances", subdir, smallestDirectory).toFile()
					.getAbsolutePath());
		}

		List<CellGroup> allStartingCellGroups = new ArrayList<>();
		for (String directory : dataDirectories) {
			Optional<CellGroup> parsedCellGroup = CellGroupParser.parseCellGroupFromFolder(directory);
			if (parsedCellGroup.isPresent()) {
				allStartingCellGroups.add(parsedCellGroup.get());
			}
		}
		return allStartingCellGroups;
	}

	/**
	 * 
	 * @param directoryPath      The path to a directory containing the wanted
	 *                           Workbook
	 * @param workbookIdentifier A string identifying the wanted workbook, e.g.
	 *                           "AllPoints" or "FinalFaces"
	 * @return The first Sheet contained in the workbook with given identifier
	 */
	private static Optional<Sheet> getFirstSheetFromWorkbookByName(String directoryPath, String workbookIdentifier) {
		File directory = new File(directoryPath);
		File[] files = directory.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.contains(workbookIdentifier) && name.endsWith(".xlsx");
			}
		});

		Sheet output = null; // only assigned null because it is contained in an Optional later

		try {
			Workbook workbook = WorkbookFactory.create(files[0]);
			output = workbook.getSheetAt(0);
			workbook.close();

		} catch (IOException e) {
			e.printStackTrace();

		}

		return Optional.ofNullable(output);
	}

	/**
	 * 
	 * @param points List of all points of a cellgroup
	 * @param id     Id of the point that is being searched for
	 * @return The point with given id
	 */
	public static Point findPointById(List<Point> points, int id) {
		return points.stream().filter(point -> (point.getId() == id)).findFirst().orElse(null);
	}

	/**
	 * 
	 * @param points List of all points of a cellgroup
	 * @param id     Id of the point that is being searched for
	 * @return The point with given id
	 */
	public static Point findPointById(Set<Point> points, int id) {
		return points.stream().filter(point -> (point.getId() == id)).findFirst().orElse(null);
	}

	/**
	 * Used to find the centerpoint of a cell by comparing its triangles.
	 * 
	 * @param triangles All triangles of one cell
	 * @param cellId    Id of the cell that contains the triangles
	 * @return The Point that all triangles of a cell share
	 */
	private static Point findCenterPointOfTriangles(List<Triangle> triangles, int cellId) {
		Point p1 = triangles.get(0).getPoints().get(0);
		Point p2 = triangles.get(0).getPoints().get(1);
		Point p3 = triangles.get(0).getPoints().get(2);
		List<Point> pointsSecondTriangle = triangles.get(1).getPoints();
		List<Point> pointsThirdTriangle = triangles.get(2).getPoints();
		if (pointsSecondTriangle.contains(p1) && pointsThirdTriangle.contains(p1)) {
			return p1;
		} else if (pointsSecondTriangle.contains(p2) && pointsThirdTriangle.contains(p2)) {
			return p2;
		} else if (pointsSecondTriangle.contains(p3) && pointsThirdTriangle.contains(p3)) {
			return p3;
		} else {
			throw new CellParserException(
					"Format of input data is wrong, there was no center point found for triangles of cell with cellId = "
							+ cellId);
		}
	}
}
