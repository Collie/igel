package it.collmann.igel.generator;

import java.util.Random;

import it.collmann.igel.model.OpheusdenCell;
import it.collmann.igel.model.Parameters;
import it.collmann.igel.model.Point;

public class CellGroupGenerator {

	private Random random;
	private Parameters parameters;

	public CellGroupGenerator() {
		this.random = new Random();
		this.parameters = Parameters.createDefault();
	}

	/**
	 * Generates a new OpheusdenCell with normal distributed venter point and
	 * radius. The values for the nextGaussian were calculated with previous given
	 * examples of cell groups.
	 * 
	 * @return A newly created OpheusdenCell
	 */
	public OpheusdenCell createRandomOpheusdenCell() {
		int newX = (int) (random.nextGaussian() * 85.14 + 223.33); // standard deviation of 85.14 and average 223.33
		int newY = (int) (random.nextGaussian() * 84.52 + 220.14); // standard deviation of 84.52 and average 220.14
		int newZ = (int) (random.nextGaussian() * 16.54 + 48.3); // standard deviation of 16.54 and average 48.3

		double minSize = parameters.minRadius;
		double maxSize = parameters.maxRadius;

		double randomRadius = (random.nextDouble() * (maxSize - minSize)) + minSize;
		Point randomCenter = new Point(1, newX, newY, newZ);
		return new OpheusdenCell(randomCenter, randomRadius);
	}
}
