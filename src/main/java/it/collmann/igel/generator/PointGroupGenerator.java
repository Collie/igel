package it.collmann.igel.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import it.collmann.igel.model.Point;

/**
 * Points that match criteria for realistic cell groups. Cell groups consist of
 * between 145 to 180 different cells. The points used here are not part of the
 * cells themselves, only meant as a general indicator of the new cell
 * locations.
 * 
 * @author collie
 *
 */
public final class PointGroupGenerator {

	private List<Point> points;
	private Random random;
	private Point first;

	private int pointDistanceMin;
	private int pointDistanceMax;

	public PointGroupGenerator(int pointDistanceMin, int pointDistanceMax) {
		this.points = new ArrayList<>();
		this.random = new Random();
		this.pointDistanceMax = pointDistanceMax;
		this.pointDistanceMin = pointDistanceMin;
	}

	/**
	 * If no pointDistanceMin / pointDistanceMax are given, use default values.
	 */
	public PointGroupGenerator() {
		this.points = new ArrayList<>();
		this.random = new Random();
		this.pointDistanceMax = 23;
		this.pointDistanceMin = 12;
	}

	/**
	 * Creates a new instance of points that fulfil criteria for realistic cell
	 * distribution.
	 * 
	 * @param numberOfCells integer for wanted number of generated cells
	 */
	public List<Point> createNewCenterPointGroup(int numberOfCells) {

		// Has to be multiplied to account for border cells being removed
		int updatedNumberOfPoints;

		if (numberOfCells <= 50) {
			updatedNumberOfPoints = (int) (numberOfCells * 2);
		} else if (numberOfCells > 50 && numberOfCells <= 75) {
			updatedNumberOfPoints = (int) (numberOfCells * 1.4);
		} else if (numberOfCells > 75 && numberOfCells <= 100) {
			updatedNumberOfPoints = (int) (numberOfCells * 1.1);
		} else {
			updatedNumberOfPoints = numberOfCells; // multiplicationFactor = 1.0
		}

		int counter = 1;
		boolean timedOut = false;
		long startTime = System.nanoTime();

		while (counter <= updatedNumberOfPoints && !timedOut) {
			if (generateNewPoint(counter)) {
				counter++;
			}

			long endTime = System.nanoTime();
			timedOut = TimeUnit.NANOSECONDS.toSeconds(endTime - startTime) > 20;
		}
		return points;
	}

	private boolean generateNewPoint(int id) {
		boolean distanceIsEnough = false;
		boolean notTimedOut = true;
		boolean negative = true;
		long startTime = System.nanoTime();

		while ((!distanceIsEnough || negative) && notTimedOut) {
			int newX = (int) (random.nextGaussian() * 85.14 + 223.33); // standard deviation of 85.14 and average 223.33
			int newY = (int) (random.nextGaussian() * 84.52 + 220.14); // standard deviation of 84.52 and average 220.14
			int newZ = (int) (random.nextGaussian() * 16.54 + 48.3); // standard deviation of 16.54 and average 48.3
			negative = newX < 0 || newY < 0 || newZ < 0;
			Point newPoint = new Point(id, newX, newY, newZ);
			if (pointHasEnoughDistanceToOtherPoints(newPoint) && !negative) {
				distanceIsEnough = true;
				points.add(newPoint);
			}

			long endTime = System.nanoTime();
			notTimedOut = TimeUnit.NANOSECONDS.toSeconds(endTime - startTime) < 2;
		}

		return distanceIsEnough && notTimedOut;
	}

	private boolean pointHasEnoughDistanceToOtherPoints(Point newPoint) {
		if (this.points.isEmpty()) {
			this.first = newPoint;
			return true;
		}

		double minDist = 10000;

		for (Point oldPoint : points) {
			int distanceX = newPoint.getX() - oldPoint.getX();
			int distanceY = newPoint.getY() - oldPoint.getY();
			int distanceZ = newPoint.getZ() - oldPoint.getZ();

			double euclideanDistance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
			if (euclideanDistance < minDist) {
				minDist = euclideanDistance;
			}

			if (euclideanDistance < pointDistanceMin) {
				return false;
			}
		}

		return minDist < pointDistanceMax;
	}
}
