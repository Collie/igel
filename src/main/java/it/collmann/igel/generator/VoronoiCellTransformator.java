package it.collmann.igel.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.kynosarges.tektosyne.geometry.LineD;
import org.kynosarges.tektosyne.geometry.PointD;
import org.kynosarges.tektosyne.geometry.Voronoi;
import org.kynosarges.tektosyne.geometry.VoronoiEdge;
import org.kynosarges.tektosyne.geometry.VoronoiResults;

import it.collmann.igel.model.Cell;
import it.collmann.igel.model.CellConnection;
import it.collmann.igel.model.CellGroup;
import it.collmann.igel.model.CellMatching;
import it.collmann.igel.model.Edge;
import it.collmann.igel.model.Point;
import it.collmann.igel.model.Triangle;
import it.collmann.igel.parser.CellGroupParser;

/**
 * Can be used to transform a cluster of Points to a group of cells with certain
 * criteria using a voronoi tesselation approach
 *
 * @author collie
 *
 */
public class VoronoiCellTransformator {

	/**
	 * 
	 * @param inputPoints List of Points that are to be made into Voronoi Diagram
	 */
	public VoronoiCellTransformator(List<Point> inputPoints, String name) {
		this.inputPoints = inputPoints;
		this.allPointsBeforeCellCleanUp = new HashSet<>(inputPoints);
		this.name = name;
		this.map1 = new HashMap<>();
		this.map2 = new HashMap<>();
		this.map3 = new HashMap<>();
		this.mapPointsCellId = new HashMap<>();
	}

	private Set<Point> allPointsBeforeCellCleanUp;

	private CellGroup generatedCellGroup;

	private List<Point> inputPoints;

	private CellMatching cellMatching;

	private String name;

	private Map<Integer, Integer> pointsMapping;

	private Map<Point, PointD> map1; // input Points to PointsD from line 78
	private Map<PointD, Integer> map2; // delaunay-Edge points to new generated Cells, line 104 and 111
	private Map<Integer, Integer> map3; // map after cell cleanup for cell IDs
	private Map<Point, Integer> mapPointsCellId; // map input points to result cell ids

	/**
	 * Creates a CellGroup via voronoi tesselation of points given. The created
	 * cells are then transformed into CellGroup format by creating the necessary
	 * triangles and new points. Only make possible by kynosarges.org, prodiving the
	 * great package Tektosyne for geometric operations like voronoi tesselation.
	 */
	public CellGroup createVoronoiTesselatedCellGroup(int numberOfRoundsOnCellGroup) {
		this.mapPointsCellId = new HashMap<>();

		Map<PointD, Point> map = new HashMap<>();

		PointD[] pointsD = new PointD[inputPoints.size()];
		for (int i = 0; i < inputPoints.size(); i++) {
			pointsD[i] = new PointD(inputPoints.get(i).getX(), inputPoints.get(i).getY());
			map1.put(inputPoints.get(i), pointsD[i]);
		}

		VoronoiResults result = Voronoi.findAll(pointsD);
		VoronoiEdge[] voronoiEdges = result.voronoiEdges;
		PointD[] voronoiVertices = result.voronoiVertices;
		LineD[] delaunayEdges = result.delaunayEdges();

		int cellIdCounter = 1;
		int newPointIdCounter = 1;
		for (LineD line : delaunayEdges) {
			map.put(line.start,
					new Point(newPointIdCounter, (int) line.start.x, (int) line.start.y, generateZCoordinate()));

			newPointIdCounter++;
			map.put(line.end, new Point(newPointIdCounter, (int) line.end.x, (int) line.end.y, generateZCoordinate()));

			newPointIdCounter++;

		}

		Map<Point, Cell> mapCellCenters = new HashMap<>();

		for (int i = 0; i < voronoiEdges.length; i++) {
			Cell cell1 = mapCellCenters.get(map.get(delaunayEdges[i].start));
			if (cell1 == null) {
				cell1 = new Cell(cellIdCounter, new ArrayList<>(), map.get(delaunayEdges[i].start));
				cellIdCounter++;
				mapCellCenters.put(map.get(delaunayEdges[i].start), cell1);
			}

			Cell cell2 = mapCellCenters.get(map.get(delaunayEdges[i].end));
			if (cell2 == null) {
				cell2 = new Cell(cellIdCounter, new ArrayList<>(), map.get(delaunayEdges[i].end));
				cellIdCounter++;
				mapCellCenters.put(map.get(delaunayEdges[i].end), cell2);
			}

			map2.put(delaunayEdges[i].start, cell1.getId());
			map2.put(delaunayEdges[i].end, cell2.getId());

			Point dStart = map.get(delaunayEdges[i].start);
			Point dEnd = map.get(delaunayEdges[i].end);

			PointD voronoiEdgeStartD = voronoiVertices[voronoiEdges[i].vertex1];
			PointD voronoiEdgeEndD = voronoiVertices[voronoiEdges[i].vertex2];

			Point vStart;
			if (map.get(voronoiEdgeStartD) == null) {
				vStart = new Point(newPointIdCounter, (int) voronoiEdgeStartD.x, (int) voronoiEdgeStartD.y,
						generateZCoordinate());
				newPointIdCounter++;

				map.put(voronoiEdgeStartD, vStart);
			} else {
				vStart = map.get(voronoiEdgeStartD);

			}
			Point vEnd;
			if (map.get(voronoiEdgeEndD) == null) {
				vEnd = new Point(newPointIdCounter, (int) voronoiEdgeEndD.x, (int) voronoiEdgeEndD.y,
						generateZCoordinate());
				newPointIdCounter++;
				map.put(voronoiEdgeEndD, vEnd);
			} else {
				vEnd = map.get(voronoiEdgeEndD);
			}

			cell1.addTriangle(new Triangle(dStart, vEnd, vStart));
			cell2.addTriangle(new Triangle(dEnd, vStart, vEnd));
		}

		List<Cell> cells = new ArrayList<>(mapCellCenters.values());
		cells = cleanUpCells(cells);

		Set<Point> allPoints = createAllPointsFromCells(cells);

		CellGroup newGroup = new CellGroup(cells, name, CellGroupParser.findAllCellConnections(allPoints, cells),
				allPoints, 0);
		newGroup.setAllPoints(setPointCellReferences(newGroup.getAllPoints(), newGroup.getCells()));
		newGroup.setCellConnections(CellGroupParser.findAllCellConnections(newGroup.getAllPoints(), cells));
		setPointCellIdMap();
		newGroup = cleanUpPoints(newGroup);
		return newGroup;
	}

	private static boolean cellisOpen(Cell cell) {
		for (Triangle triangle : cell.getTriangles()) {
			if (!triangleHasTwoNeighbors(cell.getTriangles(), triangle, cell.getCenter())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * After cells have been removed, all other cell must be adapted so that the
	 * outcome file is clean.
	 *
	 * @param cells a List
	 * @return A cellGroup where the points have been adapted.
	 */
	private List<Cell> cleanUpCells(List<Cell> cells) {
		List<Cell> filteredCells = removeIncompleteBorderCells(cells);
		List<Cell> filteredCells2 = cells.stream().filter(c -> !cellIsBorderCell(c, filteredCells))
				.collect(Collectors.toList());
		for (int i = 0; i < filteredCells2.size(); i++) {

			Cell cell = filteredCells2.get(i);
			map3.put(filteredCells2.get(i).getId(), i + 1);

			cell.setId(i + 1);
			filteredCells2.set(i, cell);
		}
		return filteredCells2;

	}

	/**
	 * After points have been removed, all other points must be adapted so that the
	 * outcome file is clean.
	 *
	 * @param cellGroup
	 * @return A cellGroup where the points have been adapted.
	 */
	private CellGroup cleanUpPoints(CellGroup cellGroup) {
		List<Point> allPoints = new ArrayList<>(cellGroup.getAllPoints());
		this.pointsMapping = new HashMap<Integer, Integer>();

		for (int i = 0; i < allPoints.size(); i++) {
			Point point = allPoints.get(i);

			this.pointsMapping.put(allPoints.get(i).getId(), i + 1);

			point.setId(i + 1);
			allPoints.set(i, point);

			for (Cell cell : cellGroup.getCells().stream().filter(c -> point.getReferencedCellIds().contains(c.getId()))
					.collect(Collectors.toList())) {

				cell.updateCellPoints(point);

			}

			for (CellConnection conn : cellGroup.getCellConnections()) {
				conn.getCell1().updateCellPoints(point);
				conn.getCell2().updateCellPoints(point);
			}
		}
		cellGroup.setAllPoints(new HashSet<>(allPoints));
		return cellGroup;

	}

	private static Set<Point> createAllPointsFromCells(List<Cell> cells) {
		Set<Point> allPoints = new HashSet<>();
		for (Cell cell : cells) {
			allPoints.add(cell.getCenter());
			for (Triangle triangle : cell.getTriangles()) {
				for (Point p : triangle.getPoints()) {
					allPoints.add(p);
				}
			}
		}
		return allPoints;
	}

	private static int generateZCoordinate() {
		return 0; // set to zero so that the instances do not mess with matching algorithms
	}

	private void setPointCellIdMap() {
		for (Point point : map1.keySet()) {
			PointD pointD = map1.get(point);
			if (pointD != null) {
				Integer id1 = map2.get(pointD);
				if (id1 != null) {
					Integer id2 = map3.get(id1);
					if (id2 != null) {
						this.mapPointsCellId.put(point, id2);
					}
				}
			}
		}
	}

	private static List<Cell> removeIncompleteBorderCells(List<Cell> cells) {
		return cells.stream().filter(c -> !cellisOpen(c)).collect(Collectors.toList());
	}

	private static boolean cellIsBorderCell(Cell cell, List<Cell> allCells) {
		List<Edge> matchingEdges = new ArrayList<>();
		for (Triangle triangle : cell.getTriangles()) {
			Edge nonCenterEdge = triangle.getNonCenterEdge(cell.getCenter());
			for (Cell otherCell : allCells) {
				if (!otherCell.equals(cell)) {
					for (Triangle otherTriangle : otherCell.getTriangles()) {
						if (otherTriangle.getNonCenterEdge(otherCell.getCenter()).equals(nonCenterEdge)) {
							matchingEdges.add(nonCenterEdge);
						}
					}
				}
			}
		}
		return matchingEdges.size() != cell.getTriangles().size();
	}

	private static Set<Point> setPointCellReferences(Set<Point> points, List<Cell> cells) {
		Set<Point> allPoints = points;
		for (Cell cell : cells) {
			Point updatedCenter = CellGroupParser.findPointById(allPoints, cell.getCenter().getId());
			updatedCenter.addCellReference(cell.getId());
			for (Triangle triangle : cell.getTriangles()) {
				for (Point point : triangle.getPoints()) {
					Point updatedPoint = CellGroupParser.findPointById(allPoints, point.getId());
					updatedPoint.addCellReference(cell.getId());
				}
			}
		}
		return allPoints;
	}

	private static boolean triangleHasTwoNeighbors(List<Triangle> triangles, Triangle triangle, Point center) {
		try {
			Point centerPoint = triangle.getPoints().stream().filter(p -> p.getId() == center.getId()).findFirst()
					.get();
			Point p1 = triangle.getPoints().stream().filter(p -> !p.equals(centerPoint)).findFirst().get();
			Point p2 = triangle.getPoints().stream().filter(p -> !p.equals(centerPoint) && !p.equals(p1)).findFirst()
					.get();
			Optional<Triangle> neighbor1 = triangles.stream()
					.filter(t -> !t.equals(triangle) && t.getPoints().contains(p1)).findFirst();
			Optional<Triangle> neighbor2 = triangles.stream()
					.filter(t -> !t.equals(triangle) && t.getPoints().contains(p2)).findFirst();
			return (neighbor1.isPresent() && neighbor2.isPresent());
		} catch (Exception e) {
			return true;
		}
	}

	/**
	 * @return the allPointsBeforeCellCleanUp
	 */
	public Set<Point> getAllPointsBeforeCellCleanUp() {
		return allPointsBeforeCellCleanUp;
	}

	/**
	 * @param allPointsBeforeCellCleanUp the allPointsBeforeCellCleanUp to set
	 */
	public void setAllPointsBeforeCellCleanUp(Set<Point> allPointsBeforeCellCleanUp) {
		this.allPointsBeforeCellCleanUp = allPointsBeforeCellCleanUp;
	}

	/**
	 * @return the generatedCellGroup
	 */
	public CellGroup getGeneratedCellGroup() {
		return generatedCellGroup;
	}

	/**
	 * @param generatedCellGroup the generatedCellGroup to set
	 */
	public void setGeneratedCellGroup(CellGroup generatedCellGroup) {
		this.generatedCellGroup = generatedCellGroup;
	}

	/**
	 * @return the inputPoints
	 */
	public List<Point> getInputPoints() {
		return inputPoints;
	}

	/**
	 * @param inputPoints the inputPoints to set
	 */
	public void setInputPoints(List<Point> inputPoints) {
		this.inputPoints = inputPoints;
	}

	/**
	 * @return the cellMatching
	 */
	public CellMatching getCellMatching() {
		return cellMatching;
	}

	/**
	 * @param cellMatching the cellMatching to set
	 */
	public void setCellMatching(CellMatching cellMatching) {
		this.cellMatching = cellMatching;
	}

	/**
	 * @return the pointsMapping
	 */
	public Map<Integer, Integer> getPointsMapping() {
		return pointsMapping;
	}

	/**
	 * @param pointsMapping the pointsMapping to set
	 */
	public void setPointsMapping(Map<Integer, Integer> pointsMapping) {
		this.pointsMapping = pointsMapping;
	}

	/**
	 * @return the mapPointsCellId
	 */
	public Map<Point, Integer> getMapPointsCellId() {
		return mapPointsCellId;
	}

	/**
	 * @param mapPointsCellId the mapPointsCellId to set
	 */
	public void setMapPointsCellId(Map<Point, Integer> mapPointsCellId) {
		this.mapPointsCellId = mapPointsCellId;
	}
}
