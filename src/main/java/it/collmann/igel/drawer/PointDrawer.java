package it.collmann.igel.drawer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.imageio.ImageIO;

import it.collmann.igel.model.Hours;
import it.collmann.igel.model.Point;

public class PointDrawer {
	/**
	 * Draws a group of points
	 * 
	 * @param pointGroup
	 * @return The created image
	 */
	public static BufferedImage drawPoints(List<Point> points, String name, Path path, Hours hours) {
		BufferedImage image = new BufferedImage(5000, 5000, BufferedImage.TYPE_BYTE_BINARY);
		Graphics2D graphics = image.createGraphics();
		graphics.setPaint(Color.WHITE);
		graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
		graphics.setStroke(new BasicStroke(2));
		graphics.setPaint(Color.BLACK);

		for (Point point : points) {
			graphics.fillOval(point.getX() * 10, point.getY() * 10, 20, 20);
		}

		try {

			Paths.get(path.toString(), name, name + "_" + hours.toString(),
					name + "_" + hours.toString() + "_PointsDrawing.png");

			Path pathForImage = Paths.get(path.toString(), name, name + "_PointDrawing.png");

			Files.deleteIfExists(pathForImage);
			Files.createDirectories(pathForImage);
			File output = new File(pathForImage.toString());
			ImageIO.write(image, "png", output);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return image;
	}
}
