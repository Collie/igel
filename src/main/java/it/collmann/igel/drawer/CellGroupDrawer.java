package it.collmann.igel.drawer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import it.collmann.igel.model.Cell;
import it.collmann.igel.model.CellConnection;
import it.collmann.igel.model.CellGroup;
import it.collmann.igel.model.Edge;
import it.collmann.igel.model.OpheusdenCell;
import it.collmann.igel.model.OpheusdenCellConnection;
import it.collmann.igel.model.OpheusdenCellGroup;
import it.collmann.igel.model.Point;
import it.collmann.igel.model.Triangle;

/**
 * A class for drawing cell groups. All created drawings are saved in
 * target/generated in the correct folder belonging to the cell group instance.
 * 
 * @author collie
 *
 */
public final class CellGroupDrawer {
	private CellGroupDrawer() {

	}

	/**
	 * Draws a cellgroup by drawing all edges of triangles that are not connected to
	 * the center point of a cell.
	 * 
	 * @param cellGroup
	 * @param flagPoints          True if the center points of all cells should be
	 *                            drawn
	 * @param flagTriangles       True if the triangles should be drawn in each cell
	 *                            with the cell edges highlighted.
	 * @param flagCellConnections True if the Connection-graph between cells should
	 *                            be drawn
	 * @param flagCellIds         True if the Cell-Ids should be drawn inside cells
	 * 
	 * @return The created image
	 */
	public static BufferedImage drawCellGroup(CellGroup cellGroup, boolean flagPoints, boolean flagTriangles,
			boolean flagCellConnections, boolean flagCellIds, Path path) {
		BufferedImage image = new BufferedImage(5000, 5000, BufferedImage.TYPE_3BYTE_BGR);
		Graphics2D graphics = image.createGraphics();
		graphics.setPaint(Color.WHITE);
		graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
		graphics.setStroke(new BasicStroke(2));
		graphics.setPaint(Color.BLACK);

		if (flagTriangles) {
			for (Cell cell : cellGroup.getCells()) {
				for (Triangle triangle : cell.getTriangles()) {
					Edge edge1 = makeEdge(triangle.getPoints().get(0), triangle.getPoints().get(1));
					Edge edge2 = makeEdge(triangle.getPoints().get(1), triangle.getPoints().get(2));
					Edge edge3 = makeEdge(triangle.getPoints().get(2), triangle.getPoints().get(0));

					graphics.drawLine(edge1.getStartPoint().getX() * 10, edge1.getStartPoint().getY() * 10,
							edge1.getEndPoint().getX() * 10, edge1.getEndPoint().getY() * 10);
					graphics.drawLine(edge2.getStartPoint().getX() * 10, edge2.getStartPoint().getY() * 10,
							edge2.getEndPoint().getX() * 10, edge2.getEndPoint().getY() * 10);
					graphics.drawLine(edge3.getStartPoint().getX() * 10, edge3.getStartPoint().getY() * 10,
							edge3.getEndPoint().getX() * 10, edge3.getEndPoint().getY() * 10);
				}
			}
		}

		Set<Edge> structuralEdges = findStructuralEdges(cellGroup);

		for (Edge edge : structuralEdges) {
			int x1 = edge.getStartPoint().getX() * 10;
			int x2 = edge.getEndPoint().getX() * 10;
			int y1 = edge.getStartPoint().getY() * 10;
			int y2 = edge.getEndPoint().getY() * 10;

			graphics.setStroke(new BasicStroke(7));

			graphics.drawLine(x1, y1, x2, y2);
		}

		graphics.setStroke(new BasicStroke(2));

		if (flagCellIds) {
			for (Cell cell : cellGroup.getCells()) {
				graphics.setFont(new Font("TimesRoman", Font.BOLD, 50));
				graphics.drawString("" + cell.getId(), cell.getCenter().getX() * 10, cell.getCenter().getY() * 10);
			}
		}

		if (flagCellConnections) {
			for (CellConnection connection : cellGroup.getCellConnections()) {
				int x1 = connection.getCell1().getCenter().getX() * 10;
				int y1 = connection.getCell1().getCenter().getY() * 10;
				int x2 = connection.getCell2().getCenter().getX() * 10;
				int y2 = connection.getCell2().getCenter().getY() * 10;
				graphics.setStroke(new BasicStroke(7));
				graphics.setPaint(Color.GRAY);
				graphics.drawLine(x1, y1, x2, y2);
			}
			graphics.setPaint(Color.BLACK);
			graphics.setStroke(new BasicStroke(2));

		}

		if (flagPoints) {
			for (Cell cell : cellGroup.getCells()) {
				Point point = cell.getCenter();
				graphics.fillOval(point.getX() * 10, point.getY() * 10, 20, 20);
			}
		}

		try {
			Path pathForImage = Paths.get(path.toString(), cellGroup.getName(),
					cellGroup.getName() + "_" + cellGroup.getHours().toString(),
					cellGroup.getName() + "_" + cellGroup.getHours().toString() + "_Drawing.png");
			Files.deleteIfExists(pathForImage);
			Files.createDirectories(pathForImage);

			File output = new File(pathForImage.toString());
			ImageIO.write(image, "png", output);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return image;
	}

	public static BufferedImage drawOpheusdenCellGroup(OpheusdenCellGroup cellGroup) {
		BufferedImage image = new BufferedImage(5000, 5000, BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics = image.createGraphics();
		graphics.setPaint(Color.WHITE);
		graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
		graphics.setStroke(new BasicStroke(2));
		graphics.setPaint(Color.GREEN);

		for (Map.Entry<OpheusdenCell, List<OpheusdenCellConnection>> cellMap : cellGroup.getCellConnectionMap()
				.entrySet()) {
			OpheusdenCell cell = cellMap.getKey();
			Point center = cell.getCenter();
			// draw the cell scaled Up
			graphics.fillOval(center.getX() * 10, center.getY() * 10, (int) cell.getRadius() * 10,
					(int) cell.getRadius() * 10);
		}

		try {
			Path pathForImage = Paths.get("target", "generated", "cellGroups", cellGroup.getName(),
					cellGroup.getName() + "_Drawing.png");
			Files.deleteIfExists(pathForImage);
			Files.createDirectories(pathForImage);
			File output = new File(pathForImage.toString());
			ImageIO.write(image, "png", output);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return image;
	}

	/**
	 * Creates a Set of all structural edges. A structural Edge is an edge not
	 * connected to the center of a cell, meaning it is either connected to another
	 * cell or the outside world.
	 * 
	 * @param cellGroup
	 * @return A set of structural edges.
	 */
	private static Set<Edge> findStructuralEdges(CellGroup cellGroup) {
		Set<Edge> structuralEdges = new HashSet<>();
		for (Cell cell : cellGroup.getCells()) {
			for (Triangle triangle : cell.getTriangles()) {
				Point cellCenter = cell.getCenter();
				Edge structuralEdge = findStructuralEdgeOfSingleTriangle(triangle, cellCenter);
				structuralEdges.add(structuralEdge);
			}
		}
		return structuralEdges;
	}

	/**
	 * 
	 * @param triangle
	 * @return A new Edge containing the structural edge of the triangle. A
	 *         structural Edge is an edge not connected to the cell's center point.
	 */
	private static Edge findStructuralEdgeOfSingleTriangle(Triangle triangle, Point cellCenter) {
		List<Point> points = triangle.getPoints();
		if (points.get(0).getId() == cellCenter.getId()) {
			return makeEdge(points.get(1), points.get(2));
		} else if (points.get(2).getId() == cellCenter.getId()) {
			return makeEdge(points.get(0), points.get(1));
		} else {
			return makeEdge(points.get(0), points.get(2));
		}
	}

	/**
	 * 
	 * @param p1 A Point
	 * @param p2 A Point
	 * @return A new edge starting at the point with lower id
	 */
	private static Edge makeEdge(Point p1, Point p2) {
		if (p1.getId() > p2.getId()) {
			return new Edge(p2, p1);
		} else
			return new Edge(p1, p2);

	}

}
