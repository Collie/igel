package it.collmann.igel.statistics;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import it.collmann.igel.model.Cell;
import it.collmann.igel.model.CellConnection;
import it.collmann.igel.model.CellGroup;
import it.collmann.igel.model.Point;
import it.collmann.igel.parser.CellGroupParser;

public class StatisticsTool {

	public static void createStatisticsWorkbookAboutPointsFromAllCellGroupsSeparate() {

		List<CellGroup> cellGroups = CellGroupParser.parseAllInitialCellGroupsFromParentFolder();
		for (CellGroup cellGroup : cellGroups) {
			List<Point> centerPoints = getAllCenterPointsFromOneCellGroup(cellGroup);

			Workbook statisticsWorkbook = new XSSFWorkbook();
			Sheet dataSheet = statisticsWorkbook.createSheet(cellGroup.getName() + "_points");

			int rowCounterForPoints = 0;
			for (Point point : centerPoints) {
				Row row = dataSheet.createRow(rowCounterForPoints);

				row.createCell(0).setCellValue(point.getX());
				row.createCell(1).setCellValue(point.getY());
				row.createCell(2).setCellValue(point.getZ());
				rowCounterForPoints++;
			}

			Sheet statisticsSheet = statisticsWorkbook.createSheet("CellConnectionDistanceBetweenPoints");

			Row header = statisticsSheet.createRow(0);
			header.createCell(0).setCellValue("CellId1");
			header.createCell(1).setCellValue("CellId2");
			header.createCell(2).setCellValue("Distance");

			int rowCounterForStatistics = 1;
			for (CellConnection connection : cellGroup.getCellConnections()) {
				Row connectionRow = statisticsSheet.createRow(rowCounterForStatistics);
				connectionRow.createCell(0).setCellValue(connection.getCell1().getId());
				connectionRow.createCell(1).setCellValue(connection.getCell2().getId());
				connectionRow.createCell(2).setCellValue(connection.getDistanceBetweenCenterPoints());

				rowCounterForStatistics++;
			}

			Path directoryForStatisticsWorkbook = Paths.get("target", "generated", "statistics",
					cellGroup.getName() + "_points.xlsx");

			String directoryForStatisticsWorkbookString = directoryForStatisticsWorkbook.toFile().getAbsolutePath();

			try {
				Files.createDirectories(directoryForStatisticsWorkbook);
				Files.deleteIfExists(directoryForStatisticsWorkbook);

				FileOutputStream outputStreamAllPoints = new FileOutputStream(directoryForStatisticsWorkbookString);

				statisticsWorkbook.write(outputStreamAllPoints);

				statisticsWorkbook.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public static void createStatisticsWorkbookAboutPointsFromAllCellGroups() {
		Workbook statisticsWorkbook = new XSSFWorkbook();
		Sheet dataSheet = statisticsWorkbook.createSheet("AllPoints");
		Sheet statisticsSheet = statisticsWorkbook.createSheet("CellConnectionDistanceBetweenPoints");
		Row header = statisticsSheet.createRow(0);
		header.createCell(0).setCellValue("CellId1");
		header.createCell(1).setCellValue("CellId2");
		header.createCell(2).setCellValue("Distance");

		int rowCounterForPoints = 0;
		int rowCounterForStatistics = 1;

		List<CellGroup> cellGroups = CellGroupParser.parseAllInitialCellGroupsFromParentFolder();
		for (CellGroup cellGroup : cellGroups) {
			List<Point> centerPoints = getAllCenterPointsFromOneCellGroup(cellGroup);

			for (Point point : centerPoints) {
				Row row = dataSheet.createRow(rowCounterForPoints);
				row.createCell(0).setCellValue(point.getX());
				row.createCell(1).setCellValue(point.getY());
				row.createCell(2).setCellValue(point.getZ());
				rowCounterForPoints++;
			}

			for (CellConnection connection : cellGroup.getCellConnections()) {
				Row connectionRow = statisticsSheet.createRow(rowCounterForStatistics);
				connectionRow.createCell(0).setCellValue(connection.getCell1().getId());
				connectionRow.createCell(1).setCellValue(connection.getCell2().getId());
				connectionRow.createCell(2).setCellValue(connection.getDistanceBetweenCenterPoints());

				rowCounterForStatistics++;
			}

		}
		Path directoryForStatisticsWorkbook = Paths.get("target", "generated", "statistics",
				"Distances_Between_AllPoints.xlsx");

		String directoryForStatisticsWorkbookString = directoryForStatisticsWorkbook.toFile().getAbsolutePath();

		try {
			Files.createDirectories(directoryForStatisticsWorkbook);
			Files.deleteIfExists(directoryForStatisticsWorkbook);

			FileOutputStream outputStreamAllPoints = new FileOutputStream(directoryForStatisticsWorkbookString);

			statisticsWorkbook.write(outputStreamAllPoints);

			statisticsWorkbook.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static List<Point> getAllCenterPointsFromAListOfCellGroups(List<CellGroup> cellGroups) {
		List<Point> allCenterPointsOfCells = new ArrayList<>();
		for (CellGroup cellGroup : cellGroups) {
			for (Cell cell : cellGroup.getCells()) {
				allCenterPointsOfCells.add(cell.getCenter());
			}
		}
		return allCenterPointsOfCells;
	}

	private static List<Point> getAllCenterPointsFromOneCellGroup(CellGroup cellGroup) {
		List<Point> allCenterPointsOfCells = new ArrayList<>();
		for (Cell cell : cellGroup.getCells()) {
			allCenterPointsOfCells.add(cell.getCenter());
		}
		return allCenterPointsOfCells;
	}
}
