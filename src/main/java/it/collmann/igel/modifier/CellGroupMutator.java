package it.collmann.igel.modifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import it.collmann.igel.model.Cell;
import it.collmann.igel.model.CellGroup;
import it.collmann.igel.model.Point;
import it.collmann.igel.model.Triangle;

/**
 * This class has the ability to mutate cells by moving all their points.
 * Currently deprecated because it has to be updated. There might be bad
 * byproducts when using this.
 * 
 * @author collie
 *
 */
@Deprecated
public class CellGroupMutator {
	private Random random;
	final int wiggleMin = -2;
	final int wiggleMax = 2;
	CellGroup cellGroup;
	Map<Cell, List<Integer>> cellPolygons;

	public CellGroupMutator(CellGroup cellGroup) {
		this.random = new Random();
		this.cellGroup = cellGroup;
		this.cellPolygons = new HashMap<>();

	}

	@Deprecated
	public CellGroup mutate() {
		for (Point point : cellGroup.getAllPoints()) {
			int[] direction = getNewRandomDirection(wiggleMin, wiggleMax);
			point = move(point, direction);
		}

		for (Cell cell : cellGroup.getCells()) {
			for (Triangle triangle : cell.getTriangles()) {
				for (Point point : triangle.getPoints()) {
					int[] direction = getNewRandomDirection(wiggleMin, wiggleMax);
					point = move(point, direction);
					this.cellGroup.setAllPoints(updatePointSet(this.cellGroup.getAllPoints(), cell));
				}
			}
			updatePointsOfCell(cellGroup.getAllPoints(), cell);
		}
		return cellGroup;
	}

	private int[] getNewRandomDirection(int wiggleMin, int wiggleMax) {
		int[] direction = new int[3];
		direction[0] = random.nextInt(wiggleMax - wiggleMin) + wiggleMin;
		direction[1] = random.nextInt(wiggleMax - wiggleMin) + wiggleMin;
		direction[2] = random.nextInt(3 + 3) - 3;
		return direction;
	}

	private Point move(Point point, int[] direction) {
		point.setX(point.getX() + direction[0]);
		point.setY(point.getY() + direction[1]);
		point.setZ(point.getZ() + direction[2]);

		return point;
	}

	private Point findPoint(int id, Set<Point> points) {
		for (Point point : points) {
			if (point.getId() == id) {
				return point;
			}
		}
		return null;
	}

	private Cell updatePointsOfCell(Set<Point> allPoints, Cell cell) {
		cell.setCenter(findPoint(cell.getCenter().getId(), allPoints));
		for (Triangle triangle : cell.getTriangles()) {
			List<Point> changedPoints = new ArrayList<>();
			for (Point point : triangle.getPoints()) {
				changedPoints.add(findPoint(point.getId(), allPoints));
			}
			triangle.setPoints(changedPoints);
		}
		return cell;
	}

	private Set<Point> updatePointSet(Set<Point> allPoints, Cell cell) {
		for (Triangle triangle : cell.getTriangles()) {
			for (Point point : triangle.getPoints()) {
				allPoints.remove(findPoint(point.getId(), allPoints));
				allPoints.add(point);
			}
		}
		allPoints.remove(findPoint(cell.getCenter().getId(), allPoints));
		allPoints.add(cell.getCenter());

		return allPoints;
	}

	/*
	 * ADAPTED FROM SOURCE
	 * http://www.sunshine2k.de/coding/java/Polygon/Convex/polygon.htm polygon is a
	 * List of points. Returns true if polygon is convex, otherwise false.
	 */
	private boolean isConvex(Cell cell) {
		List<Point> polygon = new ArrayList<>();
		for (Integer id : this.cellPolygons.get(cell)) {
			polygon.add(this.cellGroup.getAllPoints().stream().filter(p -> p.getId() == id).findFirst().get());
		}
		if (polygon.size() < 3) {
			return false;
		}

		Point p;
		Point v;
		Point u;
		int res = 0;
		for (int i = 0; i < polygon.size(); i++) {
			p = polygon.get(i);
			Point tmp = polygon.get((i + 1) % polygon.size());
			int newX = tmp.getX() - p.getX();
			int newY = tmp.getY() - p.getY();
			v = new Point(0, newX, newY, 0);
			u = polygon.get((i + 2) % polygon.size());

			if (i == 0) {// in first loop direction is unknown, so save it in res
				res = u.getX() * v.getY() - u.getY() * v.getX() + v.getX() * p.getY() - v.getY() * p.getX();
			} else {
				int newres = u.getX() * v.getY() - u.getY() * v.getX() + v.getX() * p.getY() - v.getY() * p.getX();
				if ((newres > 0 && res < 0) || (newres < 0 && res > 0)) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Since the cell Points are not ordered clockwise or anticlockwise, create a
	 * clockwise order for all points. This can be used to determine if cell is
	 * convex or not.
	 * 
	 * @param cell
	 * @return The clockwise ordered list of cell points
	 */
	private List<Integer> createClockwisePolygon(Cell cell) {
		// Get the first Triangle of a cell. Look at the two points that are not the
		// center. Create order that is clockwise. For the right point, get the triangle
		// that contains this one and continue until no triangles are left.
		List<Integer> clockwisePoints = new ArrayList<>();
		Point firstPoint = getFirstPoint(cell);

		clockwisePoints.add(firstPoint.getId());
		Point currentPoint = firstPoint;
		do {
			Point nextPoint = findNextPoint(currentPoint, cell);
			clockwisePoints.add(nextPoint.getId());
			currentPoint = nextPoint;
		} while (currentPoint != firstPoint);

		return clockwisePoints;

	}

	private Point getFirstPoint(Cell cell) {
		return cell.getTriangles().get(0).getPoints().stream().filter(p -> p != cell.getCenter()).findFirst().get();
	}

	private Point findNextPoint(Point firstPoint, Cell cell) {
		List<Triangle> triangles = cell.getTriangles();

		Triangle wantedTriangle = triangles.stream()
				.filter(t -> t.getPoints().contains(firstPoint) && triangleOrderIsClockwise(cell.getCenter(),
						firstPoint, t.getPoints().stream()
								.filter(p -> !p.equals(cell.getCenter()) && !p.equals(firstPoint)).findFirst().get()))
				.findFirst().get();
		return wantedTriangle.getPoints().stream().filter(p -> !p.equals(cell.getCenter()) && !p.equals(firstPoint))
				.findFirst().get();

	}

	// source https://algs4.cs.princeton.edu/91primitives/
	private boolean triangleOrderIsClockwise(Point p1, Point p2, Point p3) {
		return ((p2.getX() - p1.getX()) * (p3.getY() - p1.getY())
				- (p3.getX() - p1.getX()) * (p2.getY() - p1.getY())) < 0;
	}
}
