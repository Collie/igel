package it.collmann.igel.modifier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import it.collmann.igel.generator.VoronoiCellTransformator;
import it.collmann.igel.model.Cell;
import it.collmann.igel.model.CellGroup;
import it.collmann.igel.model.Point;

public class CellDivider {

	public CellDivider(CellGroup cellGroup) {
		this.cellGroup1 = cellGroup;
		this.cellMatching = new HashMap<>();
		this.random = new Random();
		this.cellDivisionMapping = new HashMap<>();
		this.deletedCells = new ArrayList<>();
	}

	private CellGroup cellGroup1;
	private CellGroup cellGroup2;
	private Map<Integer, List<Integer>> cellMatching;
	private Random random;
	// private Map<Point, Point> pointMatching;
	Map<Point, List<Point>> cellDivisionMapping;

	List<Integer> deletedCells;

	/**
	 * Divides Cells according to given hour time frame and creates Matching.
	 * 
	 * Timeframes and how many celldivision should occur: 4h -> 8,125 +/- 5 8h
	 * ->16,25 +/- 10 10h -> 25 +- 10 24h -> 60 -30, +10
	 * 
	 * @param hours The number of hours that have passed
	 */
	public CellGroup divideCells(List<Cell> cells, VoronoiCellTransformator transformator, Integer inputHours,
			Optional<Integer> divisionPercentage) {
		Set<Point> oldAllPointsBeforeCleanUp = transformator.getAllPointsBeforeCellCleanUp();
		Map<Point, Integer> oldCellMapping = transformator.getMapPointsCellId();
		Map<Integer, Point> reverseOldCellMapping = new HashMap<>();
		for (Map.Entry<Point, Integer> entry : oldCellMapping.entrySet()) {
			reverseOldCellMapping.put(entry.getValue(), entry.getKey());
		}

		Set<Point> centerPoints = new HashSet<>();
		for (Cell cell : cells) {
			centerPoints.add(cell.getCenter());
		}
		int pointIdCounter = centerPoints.size() + 1;
		int divisions = getNumberOfDivisions(cells, inputHours, divisionPercentage);
		for (int i = 0; i < divisions && i < cells.size(); i++) {
			double angle = Math.toRadians(Math.random() * 360);
			int newX1 = (int) (Math.cos(angle) * 4 + cells.get(i).getCenter().getX());
			int newY1 = (int) (Math.sin(angle) * 4 + cells.get(i).getCenter().getY());

			int[] vector = new int[] { newX1 - cells.get(i).getCenter().getX(),
					newY1 - cells.get(i).getCenter().getY() };

			int newX2 = cells.get(i).getCenter().getX() - vector[0];
			int newY2 = cells.get(i).getCenter().getY() - vector[1];

			Point newPoint1 = new Point(pointIdCounter, newX1, newY1, 0);
			pointIdCounter++;
			Point newPoint2 = new Point(pointIdCounter, newX2, newY2, 0);
			pointIdCounter++;

			List<Point> listOfPointIdsForCellMapping = new ArrayList<>();
			listOfPointIdsForCellMapping.add(newPoint1);
			listOfPointIdsForCellMapping.add(newPoint2);
			this.cellDivisionMapping.put(reverseOldCellMapping.get(cells.get(i).getId()), listOfPointIdsForCellMapping);

			centerPoints.add(newPoint1);
			centerPoints.add(newPoint2);
			centerPoints.remove(cells.get(i).getCenter());
			this.deletedCells.add(cells.get(i).getId());

			// oldAllPoints is needed so that the new voronoi tesselation has the same
			// border as the old one
			oldAllPointsBeforeCleanUp = removeCenterPointFromOldPoints(oldAllPointsBeforeCleanUp, cells.get(i));
			oldAllPointsBeforeCleanUp.add(newPoint1);
			oldAllPointsBeforeCleanUp.add(newPoint2);
		}
		transformator.setInputPoints(new ArrayList<>(oldAllPointsBeforeCleanUp));
		// transformator.setCellMatching(this.cellMatching);
		this.cellGroup2 = transformator.createVoronoiTesselatedCellGroup(2);
		Map<Point, Integer> secondCellMapping = transformator.getMapPointsCellId();

		createCellMatching(oldCellMapping, secondCellMapping);
		return cellGroup2;
	}

	private void createCellMatching(Map<Point, Integer> oldCellMapping, Map<Point, Integer> newCellMapping) {

		Map<Integer, Point> reverseOldCellMapping = new HashMap<>();
		for (Map.Entry<Point, Integer> entry : oldCellMapping.entrySet()) {
			reverseOldCellMapping.put(entry.getValue(), entry.getKey());
		}

		for (Integer id : oldCellMapping.values()) {
			this.cellMatching.put(id,
					Stream.of(newCellMapping.get(reverseOldCellMapping.get(id))).collect(Collectors.toList()));
		}

		for (Point dividedPoint : this.cellDivisionMapping.keySet()) {
			List<Integer> list = new ArrayList<>();
			for (Point point : this.cellDivisionMapping.get(dividedPoint)) {
				list.add(newCellMapping.get(point));
			}
			this.cellMatching.put(oldCellMapping.get(dividedPoint), list);
		}
	}

	public int getNumberOfDivisions(List<Cell> cells, Integer hours, Optional<Integer> divisionPercentage) {
		if (divisionPercentage.isPresent()) {
			return (int) (cells.size() * divisionPercentage.get() * 0.01);
		} else {
			return (int) (hours * 1.8); // calculated average of divisions per hour
		}
	}

	public Point findPointByCoords(Collection<Point> points, Point point) {
		return points.stream()
				.filter(p -> (p.getX() == point.getX()) && (p.getY() == point.getY()) && (point.getZ() == point.getZ()))
				.findFirst().get();
	}

	public CellGroup getCellGroup1() {
		return this.cellGroup1;
	}

	public Map<Integer, List<Integer>> getCellMatching() {
		return cellMatching;
	}

	public Set<Point> removeCenterPointFromOldPoints(Set<Point> points, Cell cell) {
		points.remove(findPointByCoords(points, cell.getCenter()));
		return points;
	}

	public void setCellGroup1(CellGroup cellGroup) {
		this.cellGroup1 = cellGroup;
	}

	public void setCellGroup2(CellGroup cellGroup) {
		this.cellGroup2 = cellGroup;
	}

	public void setCellMatching(Map<Integer, List<Integer>> cellMatching) {
		this.cellMatching = cellMatching;
	}
}
