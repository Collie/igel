package it.collmann.igel.model;

import java.util.Objects;

/**
 * Here: An edge is a connection between two points. The startPoint of an edge
 * is the one with the lower id.
 * 
 * @author collie
 *
 */
public class Edge {
	private Point startPoint;
	private Point endPoint;

	public Edge(Point startPoint, Point endPoint) {
		this.startPoint = startPoint;
		this.endPoint = endPoint;
	}

	public Point getStartPoint() {
		return this.startPoint;
	}

	public Point getEndPoint() {
		return this.endPoint;
	}

	/**
	 * Edges are equivalent if start / end match, no matter the order
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		return Objects.equals(endPoint, other.endPoint) && Objects.equals(startPoint, other.startPoint)
				|| Objects.equals(endPoint, other.startPoint) && Objects.equals(startPoint, other.endPoint);
	}

}
