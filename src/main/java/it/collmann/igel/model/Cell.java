package it.collmann.igel.model;

import java.util.List;

/**
 * A cell consists of a list of triangles that contain three points (one
 * matching the center and two matching the edge points of neighbouring cells),
 * as well as an id.
 * 
 * @author collie
 *
 */
public class Cell {
	protected int id;
	protected List<Triangle> triangles;
	protected Point center;
	protected boolean isConvex;

	/**
	 * @param id        id of the cell given in FinalFaces
	 * @param triangles A List of triangles that determine the cell
	 * @param center    the point that all triangles of the cell connect at
	 */
	public Cell(int id, List<Triangle> triangles, Point center) {
		this.id = id;
		this.triangles = triangles;
		this.center = center;
	}

	public boolean isConvex() {
		return isConvex;
	}

	public void setConvex(boolean isCovex) {
		this.isConvex = isCovex;
	}

	public List<Triangle> getTriangles() {
		return this.triangles;
	}

	public Point getCenter() {
		return this.center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void addTriangle(Triangle triangle) {
		this.triangles.add(triangle);
	}

	public void setTriangles(List<Triangle> triangles) {
		this.triangles = triangles;
	}

	public void updateCellPoints(Point point) {
		if (this.getCenter().pointMatchesNoId(point)) {
			this.setCenter(point);
		}

		for (Triangle triangle : this.getTriangles()) {
			for (Point p : triangle.getPoints()) {
				if (p.pointMatchesNoId(point)) {
					p = point;
				}
			}
		}
	}

}
