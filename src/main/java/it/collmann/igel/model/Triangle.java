package it.collmann.igel.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A triangle consists of three points.
 * 
 * @author collie
 *
 */
public class Triangle {
	private List<Point> points;

	public Triangle(Point p1, Point p2, Point p3) {
		points = new ArrayList<>();
		this.points.add(p1);
		this.points.add(p2);
		this.points.add(p3);
	}

	public List<Point> getPoints() {
		return this.points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

	public Edge getNonCenterEdge(Point cellCenter) {
		Point p1 = this.points.get(0);
		Point p2 = this.points.get(1);
		Point p3 = this.points.get(2);

		if (p1.equals(cellCenter)) {
			return new Edge(p2, p3);
		} else if (p2.equals(cellCenter)) {
			return new Edge(p1, p3);
		} else {
			return new Edge(p1, p2);
		}
	}
}
