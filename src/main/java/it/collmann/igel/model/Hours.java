package it.collmann.igel.model;

public enum Hours {
	ZERO_HOURS {
		@Override
		public String toString() {
			return "0h";
		}
	},
	FOUR_HOURS {
		@Override
		public String toString() {
			return "4h";
		}
	},
	EIGHT_HOURS {
		@Override
		public String toString() {
			return "8h";
		}
	},
	TEN_HOURS {
		@Override
		public String toString() {
			return "10h";
		}
	},
	TWELVE_HOURS {
		@Override
		public String toString() {
			return "12h";
		}
	},
	SIXTEEN_HOURS {
		@Override
		public String toString() {
			return "16h";
		}
	},
	TWENTYFOUR_HOURS {
		@Override
		public String toString() {
			return "24h";
		}
	}
}
