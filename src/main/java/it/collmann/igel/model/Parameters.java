package it.collmann.igel.model;

import java.util.Random;

public class Parameters {
	public double alpha; // displacement size at division
	public double gamma; // relative growth rate
	public double delta; // displacement at equilibration
	public double epsilon; // double connectedness at division;
	public double tau; // thickness of tetrahedron
	public double k; // force constant
	public int N; // number of iterations at equilibration
	public int P_0; // width of pressure moderation
	public double s; // displacement direction at division

	public double minRadius;
	public double maxRadius;

	private Parameters() {

	}

	public static Parameters createDefault() {
		Parameters parameters = new Parameters();
		parameters.alpha = 0.4;
		parameters.gamma = 0.01;
		parameters.delta = 0.2;
		parameters.epsilon = 0.85;
		parameters.tau = 0.4;
		parameters.k = 1;
		parameters.N = 10;
		parameters.P_0 = 1;

		Random random = new Random();
		parameters.s = random.nextDouble();

		parameters.minRadius = 10;
		parameters.maxRadius = 30;
		return parameters;
	}

}
