package it.collmann.igel.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.collmann.igel.generator.CellGroupGenerator;

public class OpheusdenCellGroup {
	private String name;
	private Map<OpheusdenCell, List<OpheusdenCellConnection>> cellConnectionMap;
	private Parameters parameters;

	public OpheusdenCellGroup(String name, Parameters parameters) {
		this.parameters = parameters;
		CellGroupGenerator generator = new CellGroupGenerator();
		OpheusdenCell initialGeneratedCell = generator.createRandomOpheusdenCell();
		cellConnectionMap = new HashMap<>();
		cellConnectionMap.put(initialGeneratedCell, new ArrayList<>());

		this.name = name;
	}

	public Map<OpheusdenCell, List<OpheusdenCellConnection>> getCellConnectionMap() {
		return cellConnectionMap;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void add(OpheusdenCell cell1, OpheusdenCell cell2) {
		OpheusdenCellConnection newConnection1 = new OpheusdenCellConnection(cell1, cell2, parameters);

		List<OpheusdenCellConnection> connections1 = cellConnectionMap.get(cell1);
		connections1.add(newConnection1);
		cellConnectionMap.put(cell1, connections1);

		List<OpheusdenCellConnection> connections2 = new ArrayList<>();
		cellConnectionMap.put(cell2, connections2);

	}

	/**
	 * S Lets the forces between cells work so the cellgroup balances out
	 */
	public void equilibrate() {
		for (int i = 0; i < parameters.N; i++) {
			for (Map.Entry<OpheusdenCell, List<OpheusdenCellConnection>> cellMap : cellConnectionMap.entrySet()) {
				OpheusdenCell currentCell = cellMap.getKey();
				Point center = currentCell.getCenter();
				center.setX((int) (center.getX() + parameters.delta * currentCell.getInterparticleForce()[0]));
				center.setY((int) (center.getY() + parameters.delta * currentCell.getInterparticleForce()[1]));
				center.setZ((int) (center.getZ() + parameters.delta * currentCell.getInterparticleForce()[2]));
			}

			for (Map.Entry<OpheusdenCell, List<OpheusdenCellConnection>> cellMap : cellConnectionMap.entrySet()) {
				OpheusdenCell currentCell = cellMap.getKey();
				for (OpheusdenCellConnection connection : cellMap.getValue()) {
					connection.updateDistanceBetweenCells();
					connection.updateInteractionPotential();
					connection.updateForceBetweenCells();

				}
				updateInterparticleForceOfOneCell(currentCell);

			}
		}
		/*
		 * for (Map.Entry<OpheusdenCell, List<CellConnection>> cellMap :
		 * cellConnectionMap.entrySet()) { OpheusdenCell currentCell = cellMap.getKey();
		 * // updatePressureWithinCell(currentCell); }
		 */

	}

	/*
	 * public void updatePressureWithinCell(OpheusdenCell cell) {
	 * List<CellConnection> connections = cellConnectionMap.get(cell);
	 * for(CellConnection connection : connections) {
	 * 
	 * }
	 * 
	 * double[] distanceVector = double volumeOfCell =
	 * (4/3)*Math.PI*cell.getRadius()*cell.getRadius()*cell.getRadius(); double
	 * pressure = (-1)*(1/(3*volumeOfCell))*cell.getInterparticleForce() }
	 */

	public void updateInterparticleForceOfOneCell(OpheusdenCell cell) {
		double[] force = { 0, 0, 0 };
		List<OpheusdenCellConnection> connections = cellConnectionMap.get(cell);
		for (OpheusdenCellConnection connection : connections) {
			force[0] = force[0] + connection.getForceBetweenCells()[0];
			force[1] = force[1] + connection.getForceBetweenCells()[1];
			force[2] = force[2] + connection.getForceBetweenCells()[2];

		}

		force[0] = (-1) * force[0];
		force[1] = (-1) * force[1];
		force[2] = (-1) * force[2];

		cell.setInterparticleForce(force);
	}
}
