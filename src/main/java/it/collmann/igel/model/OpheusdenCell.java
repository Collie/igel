package it.collmann.igel.model;

import java.util.Optional;

/**
 * A cell that has similar qualities to the Cells in Opheusden's "Algorithm for
 * a particlebased growth model for plant tissues"
 * 
 * @author collie
 *
 */
public class OpheusdenCell {

	private double radius;
	private Optional<Integer> id;
	private Point center;
	private double pressure;
	private double interparticleForce[]; // F_i
	private double pressureWithinAfterRelaxation;

	public double getPressureWithinAfterRelaxation() {
		return pressureWithinAfterRelaxation;
	}

	public void setPressureWithinAfterRelaxation(double pressureWithinAfterRelaxation) {
		this.pressureWithinAfterRelaxation = pressureWithinAfterRelaxation;
	}

	public OpheusdenCell(Point center, double radius) {
		this.center = center;
		this.radius = radius;
		this.pressure = 0;
		this.interparticleForce = new double[] { 0.0, 0.0, 0.0 };
	}

	public OpheusdenCell(int id, Point center, double radius) {
		this.id = Optional.of(id);
		this.center = center;
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public Optional<Integer> getId() {
		return id;
	}

	public void setId(int id) {
		this.id = Optional.of(id);
	}

	public Point getCenter() {
		return center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public double getPressure() {
		return pressure;
	}

	public void setPressure(double pressure) {
		this.pressure = pressure;
	}

	public double[] getInterparticleForce() {
		return interparticleForce;
	}

	public void setInterparticleForce(double[] interparticleForce) {
		this.interparticleForce = interparticleForce;
	}

	public void setId(Optional<Integer> id) {
		this.id = id;
	}

}
