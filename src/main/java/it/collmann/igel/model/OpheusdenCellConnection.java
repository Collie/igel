package it.collmann.igel.model;

public class OpheusdenCellConnection {
	private OpheusdenCell originatingCell;
	private OpheusdenCell destinationCell;
	private double distanceBetweenCells; // d_ij
	private double interactionPotential; // U_ij
	private double[] forceBetweenCells; // F_ij;
	private Parameters parameters;

	public double getInteractionPotential() {
		return interactionPotential;
	}

	public void setInteractionPotential(double interactionPotential) {
		this.interactionPotential = interactionPotential;
	}

	public double[] getForceBetweenCells() {
		return forceBetweenCells;
	}

	public void setForceBetweenCells(double[] forceBetweenCells) {
		this.forceBetweenCells = forceBetweenCells;
	}

	public OpheusdenCellConnection(OpheusdenCell originatingCell, OpheusdenCell destinationCell,
			Parameters parameters) {
		this.parameters = parameters;
		this.originatingCell = originatingCell;
		this.destinationCell = destinationCell;
		this.forceBetweenCells = new double[] { 0, 0, 0 };

		updateDistanceBetweenCells();
		updateInteractionPotential();
		updateForceBetweenCells();
	}

	public OpheusdenCell getConnectedToCell() {
		return destinationCell;
	}

	public void setConnectedToCell(OpheusdenCell connectedToCell) {
		this.destinationCell = connectedToCell;
	}

	public double getDistanceBetweenCells() {
		return distanceBetweenCells;
	}

	public void setDistanceBetweenCells(double distanceBetweenCells) {
		this.distanceBetweenCells = distanceBetweenCells;
	}

	/**
	 * Keeps cells that are directly connected sticking together | r_i - r_j |where
	 * r is the real vector of position for cell i
	 */
	public void updateDistanceBetweenCells() {
		int distX = originatingCell.getCenter().getX() - destinationCell.getCenter().getX();
		int distY = originatingCell.getCenter().getY() - destinationCell.getCenter().getY();
		int distZ = originatingCell.getCenter().getZ() - destinationCell.getCenter().getZ();
		this.distanceBetweenCells = Math.sqrt((distX * distX) + (distY * distY) + (distZ * distZ));
	}

	/**
	 * U_ij = 0.5*k*(R_i + R_j - d_ij)^2
	 */
	public void updateInteractionPotential() {
		double radiusCell1 = originatingCell.getRadius();
		double radiusCell2 = destinationCell.getRadius();
		this.interactionPotential = 0.5 * parameters.k * (radiusCell1 + radiusCell2 - distanceBetweenCells)
				* (radiusCell1 + radiusCell2 - distanceBetweenCells);
	}

	public void updateForceBetweenCells() {
		int distX = originatingCell.getCenter().getX() - destinationCell.getCenter().getX();
		int distY = originatingCell.getCenter().getY() - destinationCell.getCenter().getY();
		int distZ = originatingCell.getCenter().getZ() - destinationCell.getCenter().getZ();

		double partialDerivativeX = (parameters.k * distX
				* (originatingCell.getRadius() + destinationCell.getRadius() - distanceBetweenCells))
				/ (distanceBetweenCells - 1.0);
		double partialDerivativeY = (parameters.k * distY
				* (originatingCell.getRadius() + destinationCell.getRadius() - distanceBetweenCells))
				/ (distanceBetweenCells - 1.0);
		double partialDerivativeZ = (parameters.k * distZ
				* (originatingCell.getRadius() + destinationCell.getRadius() - distanceBetweenCells))
				/ (distanceBetweenCells - 1.0);

		this.forceBetweenCells[0] = partialDerivativeX;
		this.forceBetweenCells[1] = partialDerivativeY;
		this.forceBetweenCells[2] = partialDerivativeZ;

	}

}
