package it.collmann.igel.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A Matching for a cellGroup. Values are
 * 
 * @author collie
 */
public class CellMatching {
	private Map<Integer, List<Integer>> cellMatching;

	public CellMatching() {
		this.cellMatching = new HashMap<>();
	}

	public void put(int origin, int result) {

		if (this.cellMatching.get(origin) != null) {
			this.cellMatching.get(origin).add(result);
		} else {
			List<Integer> list = new ArrayList<>();
			list.add(result);
			this.cellMatching.put(origin, list);
		}
	}

	public void addToCell(int origin, int resultCell) {
		this.cellMatching.get(origin).add(resultCell);
	}

	public void put(Integer orginalCell, List<Integer> resultCells) {
		this.cellMatching.put(orginalCell, resultCells);
	}

	public Map<Integer, List<Integer>> get() {
		return this.cellMatching;
	}

	public boolean containsValue(int id) {
		return cellMatching.values().stream().filter(m -> m.contains(id)).collect(Collectors.toList()).size() > 0;
	}
}
