package it.collmann.igel.model;

import java.util.List;
import java.util.Set;

/**
 * A CellGroup consists of a name and a list of cells.
 * 
 * @author collie
 *
 */
public class CellGroup {
	private List<Cell> cells;
	private List<CellConnection> cellConnections;
	private String name;
	private Set<Point> allPoints;
	private Integer hours;

	/**
	 * 
	 * @param cells
	 * @param name
	 * @param connections
	 * @param allPoints
	 */
	public CellGroup(List<Cell> cells, String name, List<CellConnection> connections, Set<Point> allPoints,
			Integer hours) {
		this.cells = cells;
		this.name = name;
		this.cellConnections = connections;
		this.allPoints = allPoints;
		this.hours = hours;
	}

	public List<CellConnection> getCellConnections() {
		return cellConnections;
	}

	public void setCellConnections(List<CellConnection> cellConnections) {
		this.cellConnections = cellConnections;
	}

	public Set<Point> getAllPoints() {
		return allPoints;
	}

	public void setAllPoints(Set<Point> allPoints) {
		this.allPoints = allPoints;
	}

	public List<Cell> getCells() {
		return this.cells;
	}

	public String getName() {
		return name;
	}

	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the hours
	 */
	public Integer getHours() {
		return hours;
	}

	/**
	 * @param hours the hours to set
	 */
	public void setHours(Integer hours) {
		this.hours = hours;
	}
}