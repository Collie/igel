package it.collmann.igel.model;

public class CellConnection {
	private Cell cell1;
	private Cell cell2;
	private double distanceBetweenCenterPoints;

	public CellConnection(Cell cell1, Cell cell2) {
		this.cell1 = cell1;
		this.cell2 = cell2;
		updateDistance();
	}

	public Cell getCell1() {
		return cell1;
	}

	public void setCell1(Cell cell1) {
		this.cell1 = cell1;
	}

	public Cell getCell2() {
		return cell2;
	}

	public void setCell2(Cell cell2) {
		this.cell2 = cell2;
	}

	public void updateDistance() {
		double distX = cell1.getCenter().getX() - cell2.getCenter().getX();
		double distY = cell1.getCenter().getY() - cell2.getCenter().getY();
		double distZ = cell1.getCenter().getY() - cell2.getCenter().getZ();
		this.distanceBetweenCenterPoints = Math.sqrt((distX * distX) + (distY * distY) + (distZ * distZ));
	}

	public double getDistanceBetweenCenterPoints() {
		return distanceBetweenCenterPoints;
	}
}
