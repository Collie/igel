package it.collmann.igel.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Point contains three coordinates as well as an id. These points can later
 * be used to form the triangles that make cells.
 * 
 * @author collie
 *
 */
public class Point {
	private int id;
	private int x;
	private int y;
	private int z;

	private Set<Integer> referencedCellIds;

	@Override
	public int hashCode() {
		return Objects.hash(id, referencedCellIds, x, y, z);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		return id == other.id && Objects.equals(referencedCellIds, other.referencedCellIds) && x == other.x
				&& y == other.y && z == other.z;
	}

	public Set<Integer> getReferencedCellIds() {
		return referencedCellIds;
	}

	public void addCellReference(int id) {
		this.referencedCellIds.add(id);
	}

	public void setReferencedCellIds(Set<Integer> referencedCellIds) {
		this.referencedCellIds = referencedCellIds;
	}

	public boolean pointMatchesNoId(Point p) {
		return this.getX() == p.getX() && this.getY() == p.getY() && this.getZ() == p.getZ();
	}

	/**
	 * 
	 * @param id
	 * @param x
	 * @param y
	 * @param z
	 */
	public Point(int id, int x, int y, int z) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
		this.referencedCellIds = new HashSet<>();
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public int getId() {
		return this.id;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public int getZ() {
		return this.z;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Point with id: ");
		builder.append(this.id);
		builder.append(", with x: ");
		builder.append(this.x);
		builder.append(", with y: ");
		builder.append(this.y);
		builder.append(", with z: ");
		builder.append(this.z);
		return builder.toString();
	}
}
