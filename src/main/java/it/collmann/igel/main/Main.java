package it.collmann.igel.main;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import it.collmann.igel.drawer.CellGroupDrawer;
import it.collmann.igel.generator.PointGroupGenerator;
import it.collmann.igel.generator.VoronoiCellTransformator;
import it.collmann.igel.model.CellGroup;
import it.collmann.igel.model.Point;
import it.collmann.igel.modifier.CellDivider;
import it.collmann.igel.parser.CellGroupParser;
import it.collmann.igel.parser.CellMatchingParser;

public class Main {
	public static void main(String[] args) throws IOException {
		Integer hours = getHours(args);
		int numberOfInstances = getNumberOfInstances(args);
		int numberOfCells = getNumberOfCells(args);
		int pointDistanceMin = getPointDistanceMin(args);
		int pointDistanceMax = getPointDistanceMax(args);
		String name = getName(args);
		Path pathForOutput = Paths.get(getPath(args).toString(), name);
		Optional<Integer> divisionPercentage = getDivisionPercentage(args);
		boolean visualize = getVisualize(args);

		boolean drawCellConnections = getDrawCellConnections(args);
		boolean drawCenterPoints = getDrawCenterPoints(args);
		boolean drawCellIds = getDrawCellIds(args);
		boolean drawTriangles = getDrawTriangles(args);

		for (int i = 1; i <= numberOfInstances; i++) {
			PointGroupGenerator generator = new PointGroupGenerator(pointDistanceMin, pointDistanceMax);
			List<Point> points = generator.createNewCenterPointGroup(numberOfCells);
			VoronoiCellTransformator transformator = new VoronoiCellTransformator(points, name);
			transformator.setAllPointsBeforeCellCleanUp(new HashSet<>(points));

			CellGroup cellGroup = transformator.createVoronoiTesselatedCellGroup(1);
			cellGroup.setName(name + i);
			cellGroup.setHours(0);
			if (visualize) {
				CellGroupDrawer.drawCellGroup(cellGroup, drawCenterPoints, drawTriangles, drawCellConnections,
						drawCellIds, pathForOutput);
			}
			CellGroupParser.parseCellGroupToFolder(cellGroup, pathForOutput);

			CellDivider divider = new CellDivider(cellGroup);
			CellGroup newGroup = divider.divideCells(cellGroup.getCells(), transformator, hours, divisionPercentage);

			newGroup.setName(name + i);
			newGroup.setHours(hours);
			if (visualize) {
				CellGroupDrawer.drawCellGroup(newGroup, drawCenterPoints, drawTriangles, drawCellConnections,
						drawCellIds, pathForOutput);
			}

			CellGroupParser.parseCellGroupToFolder(newGroup, pathForOutput);
			CellMatchingParser.parseMatchingToFolder(divider.getCellMatching(), cellGroup, newGroup, pathForOutput);

		}
		if (numberOfInstances == 1) {
			System.out.println("Es wurde ein Zellkomplex mit ca. " + numberOfCells
					+ " Zellen erstellt. Die Ergebnisse befinden sich in " + pathForOutput + ".");
		} else {
			System.out.println("Es wurden " + numberOfInstances + " Zellkomplexe mit jeweils ca. " + numberOfCells
					+ " Zellen erstellt. Die Ergebnisse befinden sich in " + pathForOutput + ".");
		}

	}

	/**
	 * Default: Four Hours
	 * 
	 * @param args e.g. hours=4
	 */
	private static Integer getHours(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("hours=")).findFirst();
		if (argument.isPresent()) {
			return (Integer.valueOf((argument.get().split("=")[1])));
		} else {
			return 4;
		}
	}

	/**
	 * Default: Current working directory
	 * 
	 * @param args e.g. path=/home/collie/igel/
	 */
	private static Path getPath(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("path=")).findFirst();
		if (argument.isPresent()) {
			return Paths.get(argument.get().split("=")[1]);
		} else {
			return FileSystems.getDefault().getPath("").toAbsolutePath();
		}
	}

	/**
	 * Default: 1
	 * 
	 * @param args e.g. numOfInstances=2
	 */
	private static int getNumberOfInstances(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("numOfInstances=")).findFirst();
		if (argument.isPresent()) {
			return Integer.valueOf(argument.get().split("=")[1]);
		} else {
			return 1;
		}
	}

	/**
	 * Default: true
	 * 
	 * @param args e.g. visualize
	 */
	private static boolean getVisualize(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("visualize=")).findFirst();
		if (argument.isPresent()) {
			return Boolean.valueOf(argument.get().split("=")[1]);
		} else {
			return true;
		}
	}

	/**
	 * Default: 220
	 * 
	 * @param args e.g. numOfCells=220
	 */
	private static int getNumberOfCells(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("numOfCells=")).findFirst();
		if (argument.isPresent()) {
			return Integer.valueOf(argument.get().split("=")[1]);
		} else {
			return 220;
		}
	}

	/**
	 * Default: 12
	 * 
	 * @param args e.g. pointDistanceMin=20
	 */
	private static int getPointDistanceMin(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("pointDistanceMin=")).findFirst();
		if (argument.isPresent()) {
			return Integer.valueOf(argument.get().split("=")[1]);
		} else {
			return 12;
		}
	}

	/**
	 * Default: 23
	 * 
	 * @param args e.g. pointDistanceMax=50
	 */
	private static int getPointDistanceMax(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("pointDistanceMax=")).findFirst();
		if (argument.isPresent()) {
			return Integer.valueOf(argument.get().split("=")[1]);
		} else {
			return 23;
		}
	}

	/**
	 * If more than one cellGroup, results are numbered. E.g. GEN0, GEN1 ...
	 * Default: DD-MM-YYYY-HH-MM-SS_GEN
	 * 
	 * @param args e.g. name=Test
	 */
	private static String getName(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("name=")).findFirst();
		if (argument.isPresent()) {
			return argument.get().split("=")[1];
		} else {
			String currentTime = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());
			String cellGroupName = currentTime + "_GEN";
			return cellGroupName;
		}
	}

	/**
	 * Sets the parameter for CellGroupDrawer to draw the center points of cells.
	 * Default: false
	 * 
	 * @param args e.g. drawCenterPoints=true
	 */
	private static boolean getDrawCenterPoints(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("drawCenterPoints=")).findFirst();
		if (argument.isPresent()) {
			return Boolean.valueOf(argument.get().split("=")[1]);
		} else {
			return false;
		}
	}

	/**
	 * Sets the parameter for CellGroupDrawer to draw triangle borders within cells.
	 * Default: false
	 * 
	 * @param args e.g. drawTriangles=true
	 */
	private static boolean getDrawTriangles(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("drawTriangles=")).findFirst();
		if (argument.isPresent()) {
			return Boolean.valueOf(argument.get().split("=")[1]);
		} else {
			return false;
		}
	}

	/**
	 * Sets the parameter for CellGroupDrawer to draw cell Ids within cells.
	 * Default: false
	 * 
	 * @param args e.g. drawCellIds=true
	 */
	private static boolean getDrawCellIds(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("drawCellIds=")).findFirst();
		if (argument.isPresent()) {
			return Boolean.valueOf(argument.get().split("=")[1]);
		} else {
			return false;
		}
	}

	/**
	 * Sets the parameter for CellGroupDrawer to draw cell connections between
	 * Cells. Default: false
	 * 
	 * @param args e.g. drawCellConnections=true
	 */
	private static boolean getDrawCellConnections(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("drawCellConnections=")).findFirst();
		if (argument.isPresent()) {
			return Boolean.valueOf(argument.get().split("=")[1]);
		} else {
			return false;
		}
	}

	/**
	 * Sets the parameter for CellDivider to divide a certain percentage of cells
	 * (between 0 and 100). Default: not set
	 * 
	 * @param args e.g. drawCellConnections=true
	 */
	private static Optional<Integer> getDivisionPercentage(String[] args) {
		Optional<String> argument = Arrays.stream(args).filter(s -> s.contains("divisionPercentage=")).findFirst();
		if (argument.isPresent()) {
			return Optional.of(Integer.valueOf(argument.get().split("=")[1]));
		} else {
			return Optional.empty();
		}
	}
}
