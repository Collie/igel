package it.collmann.igel.modifier;

import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import it.collmann.igel.drawer.CellGroupDrawer;
import it.collmann.igel.model.CellGroup;
import it.collmann.igel.parser.CellGroupParser;

public class CellGroupMutatorTest {

	@Test
	void testShouldMutateCellGroup() {
		Path source = Paths.get("src", "resources", "TestInstances", "SAM0", "SAM0_10h");
		Path target = Paths.get("target", "TestArtifacts", "CellGroupMutatorTest", "SAM0_10h_mutated");
		String path = source.toFile().getAbsolutePath();
		Optional<CellGroup> cellGroup = CellGroupParser.parseCellGroupFromFolder(path);
		CellGroupDrawer.drawCellGroup(cellGroup.get(), false, false, false, false, target);

		if (cellGroup.isPresent()) {
			CellGroupMutator mutator = new CellGroupMutator(cellGroup.get());
			CellGroup newGroup = mutator.mutate();
			String currentTime = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());
			newGroup.setName("SAM0_10h_mutated_" + currentTime);
			CellGroupDrawer.drawCellGroup(newGroup, false, false, false, false, target);

		} else {
			fail("CellGroup is not present.");
		}
	}
}
