package it.collmann.igel.parser;

import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import it.collmann.igel.drawer.CellGroupDrawer;
import it.collmann.igel.model.CellGroup;

class CellGroupParserTest {
	@Test
	void testShouldParseAndDrawTestInstance1Correctly() {
		Path directory = Paths.get("src", "resources", "TestInstances", "SAM0", "SAM0_0h");
		Path target = Paths.get("target", "TestArtifacts", "CellGroupParserTest", "SAM0_0h_parsedIn");

		String path = directory.toFile().getAbsolutePath();
		Optional<CellGroup> cellGroup = CellGroupParser.parseCellGroupFromFolder(path);
		if (cellGroup.isPresent()) {
			CellGroupDrawer.drawCellGroup(cellGroup.get(), false, true, false, false, target);
		} else {
			fail("CellGroup is not present.");
		}
	}

	@Test
	void testShouldParseOut() {
		Path directory = Paths.get("src", "resources", "TestInstances", "SAM0", "SAM0_10h");
		Path target = Paths.get("target", "TestArtifacts", "CellGroupParserTest", "SAM0_10h_parsedOut");
		String path = directory.toFile().getAbsolutePath();
		Optional<CellGroup> cellGroup = CellGroupParser.parseCellGroupFromFolder(path);
		if (cellGroup.isPresent()) {
			CellGroupParser.parseCellGroupToFolder(cellGroup.get(), target);
		} else {
			fail("CellGroup is not present.");
		}
	}
}
