package it.collmann.igel.drawer;

import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import it.collmann.igel.model.CellGroup;
import it.collmann.igel.parser.CellGroupParser;

public class CellGroupDrawerTest {
	@Test
	void testShouldParseAndDrawTestInstance1Correctly() {
		Path directory = Paths.get("src", "resources", "TestInstances", "SAM0", "SAM0_0h");
		Path target = Paths.get("target", "TestArtifacts", "CellGroupDrawerTest", "SAM0_0h_drawing");
		String path = directory.toFile().getAbsolutePath();
		Optional<CellGroup> cellGroup = CellGroupParser.parseCellGroupFromFolder(path);
		if (cellGroup.isPresent()) {
			CellGroup group = cellGroup.get();
			CellGroupDrawer.drawCellGroup(group, false, false, false, false, target);
		} else {
			fail("CellGroup is not present.");
		}
	}
}
