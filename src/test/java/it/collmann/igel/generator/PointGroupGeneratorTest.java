package it.collmann.igel.generator;

import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import it.collmann.igel.drawer.PointDrawer;
import it.collmann.igel.model.Cell;
import it.collmann.igel.model.CellGroup;
import it.collmann.igel.model.Hours;
import it.collmann.igel.model.Point;
import it.collmann.igel.parser.CellGroupParser;

public class PointGroupGeneratorTest {

	@Test
	void testShouldParseAndDrawTestInstance1Correctly() {
		Path directory = Paths.get("src", "resources", "TestInstances", "SAM0", "SAM0_10h");
		Path target = Paths.get("target", "TestArtifacts", "PointGroupGeneratorTest", "SAM0_10h_points_drawing");

		String path = directory.toFile().getAbsolutePath();
		Optional<CellGroup> cellGroup = CellGroupParser.parseCellGroupFromFolder(path);
		if (cellGroup.isPresent()) {
			List<Point> centerPoints = new ArrayList<>();
			for (Cell cell : cellGroup.get().getCells()) {
				centerPoints.add(cell.getCenter());
			}
			String currentTime = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());
			PointDrawer.drawPoints(centerPoints, "CenterPointsOfSAM0_10h_" + currentTime, target, Hours.TEN_HOURS);
		} else {
			fail("CellGroup is not present.");
		}
	}
}
