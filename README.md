# IGEL

IGEL ist ein Instanzgenerator für das Matching von pflanzlichen Zellkomplexen. Es werden Instanzen erstellt, die Zellkomplexe zu verschiedenen Zeitpunkten abbilden. Zusätzlich wird für jede Instanz eine Datei "Solution" erstellt, die das Matching der Zellgruppen zueinander darstellt. Mittels verschiedener Parameter kann ausgewählt werden, wie die Zellkomplexe generiert werden sollen. Im Standardmodus werden die generierten Zellgruppen im gleichen Ordner abgelegt, in dem sich die .jar-Datei befindet. Die Standardwerte für die Parameter sind unten jeweils aufgeführt.

## Build
Der Buildprozess von IGEL kann gestartet werden, indem im "igel"-Verzeichnis
```bash
mvn clean package
```
ausgeführt wird. Im neuen Unterordner "target" findet sich danach die erstellte .jar-Datei. Diese enthält alle benötigten Dependencies.

## Ausführung
Um IGEL parameterlos zu nutzen, kann die .jar-Datei über die Kommandozeile gestartet werden. Es wird eine lauffähige Java Installation benötigt, das Programm nutzt Java 1.8.
Zur Ausführung sollte man ein Terminal im gleichen Ordner wie die .jar-Datei ausführen.

```bash
java -jar igel-1.0.0.jar
```

## Beispielaufruf
```bash
java -jar igel-1.0.0.jar hours=4 numOfInstances=2 visualize=true numOfCells=300 name=GEN drawCellIds=true
```

## Parameter
Es gibt verschiedene mögliche Parameter. IGEL kann sowohl ohne, als auch mit Parametern gestartet werden. Wenn ein Parameter angegeben wird, wird der gegebene Wert verwendet, ansonsten der Standardwert.

**path** Ein Pfad zu dem Verzeichnis, in das die generierten Zellinstanzen gespeichert werden sollen.<br>
Standardwert: Verzeichnis, in dem sich die .jar-Datei befindet.<br>
Bsp.: path=/home/collie/igel/

**hours** Die zeitliche Distanz zwischen den zwei Zeitpunkten einer Instanz.<br>
Standardwert: 4.<br>
Bsp.: hours=4

**numOfInstances** Die gewünschte zu generierende Anzahl von Instanzen.<br>
Standardwert: 1<br>
Bsp.: numOfInstances=2

**visualize** Gibt an, ob es zusätzlich für jeden generierten Zellkomplex eine graphische Darstellung geben soll.<br>
Standardwert: true<br>
Bsp.: visualize=true

**numOfCells** Gibt die gewünschte Anzahl von Zellen pro Zellkomplex an. Die tatsächliche Anzahl kann etwas abweichen.
Bei Zahlen <60 und >500 können Probleme auftreten.<br>
Standardwert: 220<br>
Bsp.: numOfCells=300

**pointDistanceMin** Ein Wert für die Mindestdistanz zwischen generierten Zellmittelpunkten.<br>
Je größer der Wert, desto größer werden die Zellen.<br>
Standardwert: 12<br>
Bsp.: pointDistanceMin=15

**pointDistanceMax** Ein Wert für die maximale Distanz zwischen generierten Zellmittelpunkten.<br>
Je größer der Unterschied zwischen pointDistanceMin und pointDistanceMax, desto größer die Größenunterschiede zwischen Zellen.<br>
Standardwert: 23<br>
Bsp.: pointDistanceMax=30

**name** Eine Bezeichnung für die generierten Zellgruppen.<br>
Standardwert: Das aktuelle Datum mit Uhrzeit im Format DD-MM-YYYY-HH-MM-SS_GEN<br>
Bsp.: name=GENERIERT

**drawCenterPoints** Gibt an, ob die Mittelpunkte der Zellen in der Visualisierung gezeichnet werden sollen.<br>
Standardwert: false<br>
Bsp.: drawCenterPoints=true

**drawTriangles** Gibt an, ob die Dreiecke der Zellen in der Visualisierung gezeichnet werden sollen.<br>
Standardwert: false<br>
Bsp.: drawTriangles=true

**drawCellIds** Gibt an, ob die Ids Zellen in der Visualisierung gezeichnet werden sollen.<br>
Standardwert: false<br>
Bsp.: drawCellIds=true

**drawCellConnections** Gibt an, ob der duale Graph der Zellen (die Zellen-Verbindungen) in der Visualisierung gezeichnet werden soll.<br>
Standardwert: false<br>
Bsp.: drawCellConnections=true

**divisionPercentage** Wenn dieser Parameter gesetzt ist, teilen sich prozentual so viele Zellen wie angegeben. Werte von 0-100 sind möglich. Überschreibt die angegebenen Stunden.<br>
Standardwert: Nicht gesetzt, Teilung wird über Stunden geregelt<br>
Bsp.: divisionPercentage=50